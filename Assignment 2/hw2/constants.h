/**
 * constants.h
 * 
 * Defines constants not specific to a project.
 * Mostly includes structs and typedefs.
 * 
 * @author: Bohui Moon [bohuimoo@usc]
 */

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#include <glm/glm.hpp>


//colors
float ORANGE[] = {255/255.f, 185/255.f, 115/255.f};
float YELLOW[] = {255/255.f, 246/255.f, 100/255.f};
float BROWN[]  = {180/255.f, 125/255.f, 75/255.f};


//structs
struct Point
{
	double x;
	double y;
	double z;
};

struct Spline 
{
	int numControlPoints;
	Point *points;
};

typedef enum {
    ROTATE, TRANSLATE, SCALE
} CONTROL_STATE;


typedef struct Frame
{   
    glm::vec3 p, t, n, b;

    void set(glm::vec3 &p, glm::vec3 &t, glm::vec3 &n, glm::vec3 &b)
    {
        this->p = p;
        this->t = t;
        this->n = n;
        this->b = b;
    }

} Frame;

size_t SIZEOF_VEC3 = sizeof(glm::vec3);

#endif