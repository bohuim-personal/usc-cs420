/**
 * config.h
 *
 * Contains constants and definitions specific to hw2 program.
 * 
 * @author Bohui Moon
 */

#ifndef _HW2_CONFIG_H_
#define _HW2_CONFIG_H_


//glut
char windowTitle[512] = "CSCI 420 homework I";
int windowWidth = 1280;
int windowHeight = 720;


//catmull-rom const
double CMR_S = 0.5;
double s = 0.5;
double CMR_DELTA_U = 0.001;

uint FRAME_RATE = 15;
float FRAME_NORMAL_SCALE = 0.2f;

float TRACK_WIDTH = 0.005f;
float TRACK_OFFSET = 0.05f;


//textures
char FILE_SKY[] = "textures/sky.jpg";
char FILE_GROUND[] = "textures/ground.jpg";


//shaders
char PATH_SHADERS[] = "../openGLHelper";
char FILE_DEFAULT_VS[] = "basic.vertexShader.glsl";
char FILE_DEFAULT_FS[] = "basic.fragmentShader.glsl";
char FILE_TEXTURE_VS[] = "texture.vertexShader.glsl";
char FILE_TEXTURE_FS[] = "texture.fragmentShader.glsl";



#endif