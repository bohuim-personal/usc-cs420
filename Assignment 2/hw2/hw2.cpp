/**
 * CSCI 420 Computer Graphics, USC 
 * Assignment 2: Roller Coaster
 *
 * Student username: bohuimoo
 */

 /* ----- INCLUDES ----- */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "openGLHeader.h"
#include "glutHeader.h"

#include "constants.h"
#include "hw2_config.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "imageIO.h"
#include "openGLMatrix.h"
#include "basicPipelineProgram.h"

using namespace std;
using namespace glm;


/* ----- PLATFORM ----- */
#ifdef WIN32
    #ifdef _DEBUG
      #pragma comment(lib, "glew32d.lib")
    #else
      #pragma comment(lib, "glew32.lib")
    #endif
#endif

#ifdef WIN32
    char shaderBasePath[1024] = SHADER_BASE_PATH;
#else
    char shaderBasePath[1024] = "../openGLHelper";
#endif



/* ----- CONST ----- */
float TO_GROUND = 1.0f;
float TO_EDGE = 100.0f;
float TO_SKY = 100.0f;

vec3 CORNERS[] = {
	vec3(  TO_EDGE,  TO_EDGE,  TO_SKY	),
	vec3(  TO_EDGE,  TO_EDGE, -TO_GROUND ),
	vec3( -TO_EDGE,  TO_EDGE,  TO_SKY	),
	vec3( -TO_EDGE,  TO_EDGE, -TO_GROUND ),
	vec3( -TO_EDGE, -TO_EDGE,  TO_SKY	),
	vec3( -TO_EDGE, -TO_EDGE, -TO_GROUND ),
	vec3(  TO_EDGE, -TO_EDGE,  TO_SKY	),
	vec3(  TO_EDGE, -TO_EDGE, -TO_GROUND ),
};


/* ----- EVENT STATES ----- */
int mousePos[2]; // x,y coordinate of the mouse position

int leftMouseButton = 0; // 1 if pressed, 0 if not 
int middleMouseButton = 0; // 1 if pressed, 0 if not
int rightMouseButton = 0; // 1 if pressed, 0 if not

CONTROL_STATE controlState = ROTATE;
float landScale[3] = {1.0f, 1.0f, 1.0f};
float landRotate[3] = {0.0f, 0.0f, 0.0f};
float landTranslate[3] = {0.0f, 0.0f, 0.0f};

uint ssIndex = 0;

uint frameIndex = 0;
bool paused = false;
bool controlEnabled = false;


/* ----- VARS ----- */
BasicPipelineProgram *defaultPipeline;
BasicPipelineProgram *texturePipeline;
GLuint defaultProgram;
GLuint textureProgram;

GLuint skyTex;
GLuint groundTex;

OpenGLMatrix *matrix;

uint numSplines;
Spline *splines;

uint numFrames;
Frame *frames;

uint numTrackVertices;
GLuint trackL;
GLuint trackR;

uint numWorldVertices;
GLuint world;




/* ----- FUNCTIONS ----- */
void init(int argc, char *argv[]);
void initGLEW();
void initGLUT(int argc, char *argv[]);

void initMatrices();
void initPrograms();

int  loadTexture(const char *imageFilename, GLuint textureHandle);
void initTextureHandles();


int  loadSplines(char *trackFilename);
vec3 cmrPosition(double u, Point &p0, Point &p1, Point &p2, Point &p3);
vec3 cmrTangent(double u, Point &p0, Point &p1, Point &p2, Point &p3);
void initFrames();

//models
void fillData(GLuint *vao, size_t dataSize, const void *data, GLuint &program, const char *attrib, int attribSize);
void initWorld();
void initTrack();

//rendering
void setMatrices(float *modelView, float *projection);
void useMatrices(GLuint &program, float *modelView, float *projection);
void displayTrack(float *modelView, float *projection);
void displayWorld(float *modelView, float *projection);

//loop
void displayFunc();
void idleFunc();
void reshapeFunc(int w, int h);
void mouseMotionDragFunc(int x, int y);
void mouseMotionFunc(int x, int y);
void mouseButtonFunc(int button, int state, int x, int y);
void keyboardFunc(unsigned char key, int x, int y);

//helpers
void error(const char *message);
void saveScreenshot(const char *filename);



/* ----- INIT & SETUP ----- */
/**
 * Main function of program.
 * Must be provided with text file contains names of .sp spline files.
 */
int main(int argc, char *argv[])
{
    if (argc < 2)
  	{  
    	printf ("usage: %s <trackfile>\n", argv[0]);
    	exit(0);
  	}

    init(argc, argv);
    glutMainLoop();
}

/**
 * Call all necessary init functions
 */
void init(int argc, char *argv[])
{
    //cores
    initGLEW();
    initGLUT(argc, argv);

    //helper classes
    initPrograms();
    initMatrices();

    //external assets
    initTextureHandles();
    loadSplines(argv[1]);
    initFrames();

    //models
    initTrack();
    initWorld();
}

/**
 * Initialize GLEW depending on OS.
 */
void initGLEW()
{
    #ifdef __APPLE__
        // nothing is needed on Apple
    #else
        // Windows, Linux
        GLint result = glewInit();
        if (result != GLEW_OK)
        {
            cout << "error: " << glewGetErrorString(result) << endl;
            exit(EXIT_FAILURE);
        }
    #endif
}

/**
 * Initialize GLUT and OpenGL.
 * Set window properties and assign the main loop function selectors.
 */
void initGLUT(int argc, char *argv[])
{
    //cout << "Initializing GLUT..." << endl;
    glutInit(&argc,argv);

    //cout << "Initializing OpenGL..." << endl;
    #ifdef __APPLE__
        glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    #else
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    #endif

    glutInitWindowSize(windowWidth, windowHeight);
    glutInitWindowPosition(0, 0);  
    glutCreateWindow(windowTitle);

    //main loop selectors
    glutDisplayFunc(displayFunc); 
    glutIdleFunc(idleFunc);
    glutMotionFunc(mouseMotionDragFunc);
    glutPassiveMotionFunc(mouseMotionFunc);
    glutMouseFunc(mouseButtonFunc);
    glutReshapeFunc(reshapeFunc);
    glutKeyboardFunc(keyboardFunc);

    cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
    cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << endl;
    cout << "Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

    //extra
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
}

/**
 * Initialize pipeline helpers and initialize on appropriate shaders.
 * Also get respective program handles.
 */
void initPrograms()
{
	defaultPipeline = new BasicPipelineProgram();
	if ( defaultPipeline->Init(PATH_SHADERS, FILE_DEFAULT_VS, FILE_DEFAULT_FS) != 0 )
		error("[Error] failed to compile default shaders");

	texturePipeline = new BasicPipelineProgram();
	if ( texturePipeline->Init(PATH_SHADERS, FILE_TEXTURE_VS, FILE_TEXTURE_FS) != 0 )
		error("[Error] failed to compile texture shaders");

	defaultProgram = defaultPipeline->GetProgramHandle();
	textureProgram = texturePipeline->GetProgramHandle();
}

/**
 * Initialize aspects related to matrix operations.
 */
void initMatrices()
{
    matrix = new OpenGLMatrix();
}



/* --- TEXTURES ---*/
/**
 * Initialize texture from given image.
 *
 * @param imageFilename - path to image to use as texture
 * @param textureHandle - texture handle to load image on
 */
int loadTexture(const char *imageFilename, GLuint textureHandle)
{
  	// read the texture image
  	ImageIO img;
  	ImageIO::fileFormatType imgFormat;
  	ImageIO::errorType err = img.load(imageFilename, &imgFormat);

  	if (err != ImageIO::OK) 
  	{
    	printf("Loading texture from %s failed.\n", imageFilename);
    	return -1;
  	}

  	// check that the number of bytes is a multiple of 4
  	if (img.getWidth() * img.getBytesPerPixel() % 4) 
  	{
    	printf("Error (%s): The width*numChannels in the loaded image must be a multiple of 4.\n", imageFilename);
    	return -1;
  	}

  	// allocate space for an array of pixels
  	int width = img.getWidth();
  	int height = img.getHeight();
  	unsigned char * pixelsRGBA = new unsigned char[4 * width * height]; // we will use 4 bytes per pixel, i.e., RGBA

  	// fill the pixelsRGBA array with the image pixels
  	memset(pixelsRGBA, 0, 4 * width * height); // set all bytes to 0
  	for (int h = 0; h < height; h++)
  	{
    	for (int w = 0; w < width; w++) 
    	{
      		// assign some default byte values (for the case where img.getBytesPerPixel() < 4)
      		pixelsRGBA[4 * (h * width + w) + 0] = 0;    // red
      		pixelsRGBA[4 * (h * width + w) + 1] = 0;    // green
      		pixelsRGBA[4 * (h * width + w) + 2] = 0;    // blue
      		pixelsRGBA[4 * (h * width + w) + 3] = 255;  // alpha channel; fully opaque

      		// set the RGBA channels, based on the loaded image
      		int numChannels = img.getBytesPerPixel();
      		for (int c = 0; c < numChannels; c++) // only set as many channels as are available in the loaded image; the rest get the default value
        		pixelsRGBA[4 * (h * width + w) + c] = img.getPixel(w, h, c);
    	}
    }

  	// bind the texture
  	glBindTexture(GL_TEXTURE_2D, textureHandle);

  	// initialize the texture
  	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelsRGBA);

  	// generate the mipmaps for this texture
  	glGenerateMipmap(GL_TEXTURE_2D);

  	// set the texture parameters
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  	
  	// query support for anisotropic texture filtering
  	GLfloat fLargest;
  	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
  	printf("Max available anisotropic samples: %f\n", fLargest);
  
  	// set anisotropic texture filtering
  	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0.5f * fLargest);

  	// query for any errors
  	GLenum errCode = glGetError();
  	if (errCode != 0) 
  	{
        printf("Texture initialization error. Error code: %d.\n", errCode);
        return -1;
  	}
  
  	// de-allocate the pixel array -- it is no longer needed
    delete [] pixelsRGBA;
    return 0;
}

/**
 * Generate all necessary texture handles and load them using loadTexture()
 */
void initTextureHandles()
{
	//generate all necessary texture handles
	glGenTextures(1, &skyTex);
	glGenTextures(1, &groundTex);

	//init textures
	loadTexture(FILE_SKY, skyTex);
	loadTexture(FILE_GROUND, groundTex);

	//use texture 0
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(textureProgram, "texImage"), 0);
}



/* --- SPLINES --- */
/**
 * Initialize splines from the given text file
 *
 * @param argv - path to filename containing list of .sp spline files
 */
int loadSplines(char *argv)
{
	char *cName = (char *) malloc(128 * sizeof(char));
	FILE *fileList;
	FILE *fileSpline;
	int iType, i = 0, j, iLength;

	// load the track file 
	fileList = fopen(argv, "r");
	if (fileList == NULL) 
  	{
    	printf ("can't open file\n");
    	exit(1);
  	}
  
  	// stores the number of splines in a global variable 
  	fscanf(fileList, "%d", &numSplines);

  	splines = (Spline*) malloc(numSplines * sizeof(Spline));

  	// reads through the spline files 
  	for (j = 0; j < numSplines; j++) 
  	{
   		i = 0;
    	fscanf(fileList, "%s", cName);
    	fileSpline = fopen(cName, "r");

    	if (fileSpline == NULL) 
    	{
      		printf ("can't open file\n");
      		exit(1);
    	}

    	// gets length for spline file
    	fscanf(fileSpline, "%d %d", &iLength, &iType);

    	// allocate memory for all the points
    	splines[j].points = (Point *)malloc(iLength * sizeof(Point));
    	splines[j].numControlPoints = iLength;

    	// saves the data to the struct
    	while ( fscanf(fileSpline, "%lf %lf %lf", &splines[j].points[i].x, &splines[j].points[i].y, &splines[j].points[i].z) != EOF) 
    	{
      		i++;
    	}
  	}

    free(cName);
  	return 0;
}

/**
 * Returns position on Catmull-Rom spline at specified u, given 4 control points
 * 
 * @param u
 * @param p0 - 1st control point
 * @param p1 - 2nd control point
 * @param p2 - 3rd control point
 * @param p3 - 4th control point
 */
vec3 cmrPosition(double u, Point &p0, Point &p1, Point &p2, Point &p3)
{
	double u3 = u * u * u;
	double u2 = u * u;

	double t0 = (u3 * (-s)) + (u2 * 2 * s) + (u * (-s));
    double t1 = (u3 * (2-s)) + (u2 * (s-3)) + 1;
    double t2 = (u3 * (s-2)) + (u2 * (3 - 2*s)) + (u * s);
    double t3 = (u3 * s) - (u2 * s);

    vec3 position;
    position.x = (t0 * p0.x) + (t1 * p1.x) + (t2 * p2.x) + (t3 * p3.x);
    position.y = (t0 * p0.y) + (t1 * p1.y) + (t2 * p2.y) + (t3 * p3.y);
    position.z = (t0 * p0.z) + (t1 * p1.z) + (t2 * p2.z) + (t3 * p3.z);

    return position;
}

/**
 * Returns non-normalized vec3 of tangent at specified u, given 4 control points.
 * 
 * @param u
 * @param p0 - 1st control point
 * @param p1 - 2nd control point
 * @param p2 - 3rd control point
 * @param p3 - 4th control point
 */
vec3 cmrTangent(double u, Point &p0, Point &p1, Point &p2, Point &p3)
{
	double u2 = u * u;

	double t0 = -s + (4 * s * u) - (3 * s * u2);
    double t1 = (2 * (-3 + s) * u) + (3 * (2 - s) * u2);
    double t2 = s + (2 * (3 - 2 * s) * u) + (3 * (-2 + s) * u2);
    double t3 = (-2 * s * u) + (3 * s * u2);

	vec3 tangent;
    tangent.x = (t0 * p0.x) + (t1 * p1.x) + (t2 * p2.x) + (t3 * p3.x);
    tangent.y = (t0 * p0.y) + (t1 * p1.y) + (t2 * p2.y) + (t3 * p3.y);
    tangent.z = (t0 * p0.z) + (t1 * p1.z) + (t2 * p2.z) + (t3 * p3.z);

    return normalize(tangent);
}

void initFrames()
{
	//calculate number of frames
	numFrames = 0;
    for (uint spi = 0; spi < numSplines; ++spi)
    {
    	uint ncp = splines[spi].numControlPoints;
    	numFrames += (ncp - 3) * (1.0 / CMR_DELTA_U);
    }

    frames = new Frame[numFrames];

    uint index = 0;
	for (int sp_i = 0; sp_i < numSplines; ++sp_i)
	{
		Spline sp = splines[sp_i];

		for (int cp_i = 1; cp_i < sp.numControlPoints - 2; ++cp_i)
		{
			//get points, only section between p1 ~ p2 is actually rendered
			Point p0 = sp.points[cp_i - 1];
			Point p1 = sp.points[cp_i + 0];
			Point p2 = sp.points[cp_i + 1];
			Point p3 = sp.points[cp_i + 2];

			for (double u = 0; u <= 1.0; u += CMR_DELTA_U)
			{
				vec3 p = cmrPosition(u, p0, p1, p2, p3);
				vec3 t = cmrTangent(u, p0, p1, p2, p3);

				vec3 n = (index == 0) ? vec3(0.f, 0.f, 1.f) : normalize( cross(frames[index-1].b, t) );
				vec3 b = normalize( cross(t, n) );

				frames[index++].set(p, t, n, b);
			}
		}
	}
}


/* --- MODELS --- */
/**
 * Given the appropriate parameters, fill the VAO with data and link to shader variables.
 * 
 * @param vao 		 - pointer to GLuint vao to fill
 * @param dataSize	 - size_t of given data array
 * @param data 		 - data array for drawing arrays, not elements
 * @param program 	 - reference to program with loaded shaders
 * @param attrib 	 - in attrib for the shader
 * @param attribSize - int size of specified shader attrib
 */
void fillData(GLuint *vao, size_t dataSize, const void *data, GLuint &program, const char *attrib, int attribSize)
{
	glBindVertexArray(*vao);

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, dataSize, data, GL_STATIC_DRAW);

	GLuint loc = glGetAttribLocation(program, attrib);
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, attribSize, GL_FLOAT, GL_FALSE, 0, (void*) 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

/**
 * Return the corresponding vertex on the track cross section.
 * Indices are located at pre-determined corners of the cross section tube.
 * 
 * @param curr - current Frenet frame
 * @param next - next Frenet frame 
 * @param index - int specifing pre-determined location on track tube
 * @param left  - bool for left or right rail
 */
vec3 trackVertex(Frame &curr, Frame &next, int index, bool left)
{
	vec3 vertex;
	switch (index)
	{
		case 0: vertex = curr.p + ( curr.n + curr.b) * TRACK_WIDTH; break;
		case 1: vertex = next.p + ( next.n + next.b) * TRACK_WIDTH; break;
		case 2: vertex = curr.p + ( curr.n - curr.b) * TRACK_WIDTH; break;
		case 3: vertex = next.p + ( next.n - next.b) * TRACK_WIDTH; break;
		case 4: vertex = curr.p + (-curr.n - curr.b) * TRACK_WIDTH; break;
		case 5: vertex = next.p + (-next.n - next.b) * TRACK_WIDTH; break;
		case 6: vertex = curr.p + (-curr.n + curr.b) * TRACK_WIDTH; break;
		case 7: vertex = next.p + (-next.n + next.b) * TRACK_WIDTH; break;
		case 8: vertex = curr.p + ( curr.n + curr.b) * TRACK_WIDTH; break;
		case 9: vertex = next.p + ( next.n + next.b) * TRACK_WIDTH; break;
		default: break;
	}

	//double rail offset
	//left rail is -binormal
	//even indices use curret, odd use next frames
	vec3 offset = (left ? -1 : 1) * TRACK_OFFSET * (index % 2 == 0 ? curr.b : next.b);

	//return final
	return vertex + offset;
}

/**
 *
 */
void initTrack()
{	
    numTrackVertices = 9 * (numFrames - 1) + 1;

    vec3 *colors = new vec3[numTrackVertices];
    vec3 *verticesL = new vec3[numTrackVertices];
    vec3 *verticesR = new vec3[numTrackVertices];

    uint index = 0;
    for (uint i = 1; i < numFrames - 1; ++i)
    {
    	Frame curr = frames[i];
    	Frame next = frames[i + 1];

    	//include vertex index 0 for first track tube
    	for (uint k = (i == 0) ? 0 : 1; k < 10; ++k)
    	{
    		verticesL[index] = trackVertex(curr, next, k, true);
    		verticesR[index] = trackVertex(curr, next, k, false);

    		colors[index] = vec3(0.0f);
    		++index;
    	}
    }


    glGenVertexArrays(1, &trackL);
    fillData(&trackL, numTrackVertices * SIZEOF_VEC3, colors, defaultProgram, "color", 3);
    fillData(&trackL, numTrackVertices * SIZEOF_VEC3, verticesL, defaultProgram, "position", 3);

    glGenVertexArrays(1, &trackR);
    fillData(&trackR, numTrackVertices * SIZEOF_VEC3, colors, defaultProgram, "color", 3);
    fillData(&trackR, numTrackVertices * SIZEOF_VEC3, verticesR, defaultProgram, "position", 3);

    delete[] colors;
    delete[] verticesL;
    delete[] verticesR;
}

/**
 *
 */
void initWorld()
{
	numWorldVertices = 6 * 2 * 3; //6 sides, 2 trigles per side, 3 vertices per triangle

	vec3 *vertices = new vec3[numWorldVertices];

   	uint cornerIndex[] = {
   		//sky
    	0, 3, 1,
    	0, 3, 2,
    	2, 5, 3,
    	2, 5, 4,
    	4, 7, 5,
    	4, 7, 6,
    	6, 1, 7,
    	6, 1, 0,
    	0, 4, 6,
    	0, 4, 2,

    	//ground
    	1, 5, 7,
    	1, 5, 3
    };

    float texPoints[][2] = {
    	//sky
    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {0, 1},

    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {1, 0},

    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {1, 0},

    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {1, 0},

    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {1, 0},

    	//ground
    	{1, 1}, {0, 0}, {1, 0},
    	{1, 1}, {0, 0}, {1, 0}
    };

    for (uint i=0; i < numWorldVertices; ++i)
    	vertices[i] = CORNERS[ cornerIndex[i] ];

	glGenVertexArrays(1, &world);
    fillData(&world, numWorldVertices * SIZEOF_VEC3, vertices, textureProgram, "position", 3);
    fillData(&world, numWorldVertices * 2 * sizeof(float), texPoints, textureProgram, "texPoint", 2);
    delete[] vertices;
}



/* --- RENDERING --- */
/**
 * Calculate all matrix transformations and fill given arrays.
 * 
 * @param modelView  - pointer to float array for model view matrix
 * @param projection - pointer to float array for projection matrix
 */
void setMatrices(float *modelView, float *projection)
{
	matrix->SetMatrixMode(OpenGLMatrix::ModelView);
    matrix->LoadIdentity();

    //camera
    Frame f = frames[frameIndex];
    vec3 p = f.p;
    vec3 t = f.t;
    vec3 n = f.n;

    vec3 offset = (n * FRAME_NORMAL_SCALE);
    vec3 e = p + offset;
    vec3 c = p + t + offset;

    matrix->LookAt(e.x, e.y, e.z, c.x, c.y, c.z, n.x, n.y, n.z);

    //manual control when paused
    if (paused && controlEnabled)
    {
    	// matrix->Scale(landScale[0], landScale[1], landScale[2]);
    	matrix->Rotate(landRotate[0]/2, 1.0, 0.0, 0.0);
    	matrix->Rotate(landRotate[1]/2, 0.0, 1.0, 0.0);
    	matrix->Rotate(landRotate[2]/2, 0.0, 0.0, 1.0);
    	matrix->Translate(landTranslate[0], landTranslate[1], landTranslate[2]);
    }

    //model
    matrix->GetMatrix(modelView);

    //projection
    matrix->SetMatrixMode(OpenGLMatrix::Projection);
    matrix->GetMatrix(projection);
}

/**
 * Upload given Model-View and Projection matrices to the specified program.
 * Method will automatically call glUseProgram() on the passed program.
 *
 * @param program - reference to program
 * @param modelView - float[16] of the model view matrix
 * @param projection - float[16] of the projection matrix
 */
void useMatrices(GLuint &program, float *modelView, float *projection)
{
	glUseProgram(program);

	//model view
	GLuint mLoc = glGetUniformLocation(program, "modelViewMatrix");
	glUniformMatrix4fv(mLoc, 1, GL_FALSE, modelView);

	//projection
	GLuint pLoc = glGetUniformLocation(program, "projectionMatrix");
	glUniformMatrix4fv(pLoc, 1, GL_FALSE, projection);
}

/**
 * Bind world and render world cube.
 * Needs to be separated in two draw arrays for two textures.
 */
void displayWorld(float *modelView, float *projection)
{
	useMatrices(textureProgram, modelView, projection);

	glBindVertexArray(world);

	uint numSky = 5 * 2 * 3; //5 faces, 2 triangles per, 3 vertices per triangle
	uint numGround = 1 * 2 * 3; //1 face, 2 triangles per, 3 vertices per triangle

	glBindTexture(GL_TEXTURE_2D, skyTex);
	glDrawArrays(GL_TRIANGLES, 0, numSky);

	glBindTexture(GL_TEXTURE_2D, groundTex);
	glDrawArrays(GL_TRIANGLES, numSky, numGround);

	glBindVertexArray(0);
}

/**
 * Bind trackL and render the track.
 */
void displayTrack(float *modelView, float *projection)
{
	useMatrices(defaultProgram, modelView, projection);

	glBindVertexArray(trackL);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, numTrackVertices);

    glBindVertexArray(trackR);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, numTrackVertices);

    glBindVertexArray(0);
}



/* ----- GLUT EVENT LOOP ----- */
/**
 * Called when window is resized, which pauses display updates.
 * Based on the new window aspect, change camera perspective.
 */
void reshapeFunc(int w, int h)
{
    glViewport(0, 0, w, h);
    
    windowWidth = w;
    windowHeight = h;

    GLfloat aspect = (GLfloat) w / (GLfloat) h; 
    matrix->SetMatrixMode(OpenGLMatrix::Projection); 
    matrix->LoadIdentity(); 
    matrix->Perspective(60.0, aspect, 0.01, 1000.0);
    matrix->SetMatrixMode(OpenGLMatrix::ModelView);
}

/**
 * Idle phase before displaying.
 */
void idleFunc()
{
	if (!paused)
	{
    	if (frameIndex < numFrames - FRAME_RATE)
    		frameIndex += FRAME_RATE;
    }

    // string saveto = "screenshots/" + to_string(ssIndex++) + ".jpg";
    // saveScreenshot(saveto.c_str());

    glutPostRedisplay();
}

/**
 * Display the generated VAOs according to user interaction & landscape state.
 * Set the camera, apply transformations, and load the matrices to GPU.
 * Bind VAO according to selected render mode, call glDrawArrays(), and swap buffer.
 */
void displayFunc()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 


    float modelView[16];
    float projection[16];
    setMatrices(modelView, projection);
    
    displayWorld(modelView, projection);
    displayTrack(modelView, projection);

    glutSwapBuffers();
}

/**
 * Called when user presses a key.
 *
 * @param key - pressed key
 */
void keyboardFunc(unsigned char key, int x, int y)
{
    switch (key)
    {  
    	//reset
    	case 'r': case 'R': 
    		frameIndex = 0; 
    		break;

    	//pause
    	case 'p': case 'P': 
    		paused = !paused; 
    		break;

    	//control
    	case 'e': case 'E':
    		controlEnabled = !controlEnabled;
    		break;

        //screenshot
        case 'x': case 'X': 
        	saveScreenshot("screenshot.jpg"); 
        	break;

        //ESC: quit program
        case 27: exit(0);
    }
}

/**
 * Store new position of mouse, for calculating delta mouse move
 */
void mouseMotionFunc(int x, int y)
{
    mousePos[0] = x;
    mousePos[1] = y;
}

/**
 * When mouse button has been pressed/released.
 * Store mouse button & modifier keys (CTRL/SHIFT) states
 */
void mouseButtonFunc(int button, int state, int x, int y)
{   
    //track mouse button state: leftMouseButton, middleMouseButton, rightMouseButton
    switch (button)
    {
        case GLUT_LEFT_BUTTON:
            leftMouseButton = (state == GLUT_DOWN);
            break;
        case GLUT_MIDDLE_BUTTON:
            middleMouseButton = (state == GLUT_DOWN);
            break;
        case GLUT_RIGHT_BUTTON:
            rightMouseButton = (state == GLUT_DOWN);
        break;
    }

    // keep track of whether CTRL and SHIFT keys are pressed
    switch (glutGetModifiers())
    {
        case GLUT_ACTIVE_CTRL:
            controlState = TRANSLATE;
            break;
        case GLUT_ACTIVE_SHIFT:
            controlState = SCALE;
            break;
        default: //if CTRL and SHIFT are not pressed, we are in rotate mode
            controlState = ROTATE;
            break;
    }

    // store the new mouse position
    mousePos[0] = x;
    mousePos[1] = y;
}

/**
 * Called when mouse is moved while pressing a button.
 * Depending on both currently pressed key modifier & mouse movement, modify transformation matrix.
 */
void mouseMotionDragFunc(int x, int y)
{
    //get position delta
    int mousePosDelta[2] = { x - mousePos[0], y - mousePos[1] };

    switch (controlState)
    {
        // translate the landscape
        case TRANSLATE:
            if (leftMouseButton)
            {
                // control x,y translation via the left mouse button
                landTranslate[0] += mousePosDelta[0] * 0.01f;
                landTranslate[1] -= mousePosDelta[1] * 0.01f;
            }
            if (rightMouseButton) {
                landTranslate[2] -= mousePosDelta[1] * 0.01f;
            }
            break;
        case ROTATE:
        	if (leftMouseButton) {
        		landRotate[1] -= mousePosDelta[1];
                landRotate[2] += mousePosDelta[0];
            }
            if (rightMouseButton)
            {
                // control x,y rotation via the left mouse button
                landRotate[0] += mousePosDelta[1];
                // landRotate[1] += mousePosDelta[0];
            }
            break;
        case SCALE:
            if (leftMouseButton)
            {
                // control x,y scaling via the left mouse button
                landScale[0] *= 1.0f + mousePosDelta[0] * 0.01f;
                landScale[1] *= 1.0f - mousePosDelta[1] * 0.01f;
            }
            if (rightMouseButton) {
                landScale[2] *= 1.0f - mousePosDelta[1] * 0.01f;
            }
            break;
    }

    // store the new mouse position
    mousePos[0] = x;
    mousePos[1] = y;
}


/* ----- HELPERS ----- */
/**
 * Print the given error message and exit with failure
 */
void error(const char *message)
{
	cout << message << endl;
	exit(EXIT_FAILURE);
}

/**
 * write a screenshot to the specified filename
 */
void saveScreenshot(const char *filename)
{
    unsigned char * screenshotData = new unsigned char[windowWidth * windowHeight * 3];
    glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, screenshotData);

    ImageIO screenshotImg(windowWidth, windowHeight, 3, screenshotData);

    if (screenshotImg.save(filename, ImageIO::FORMAT_JPEG) == ImageIO::OK)
        cout << "File " << filename << " saved successfully." << endl;
    else 
        cout << "Failed to save file " << filename << '.' << endl;

    delete[] screenshotData;
}




