#version 150

in vec2 _texPoint;

out vec4 outColor;

uniform sampler2D texImage;


void main()
{
	outColor = texture(texImage, _texPoint);
}