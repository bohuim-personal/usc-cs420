Subject 	: CSCI420 - Computer Graphics 
Assignment 2: Simulating a Roller Coaster
Author		: < Insert you name here >
USC ID 		: < Insert your USC ID here >

Description: In this assignment, we use Catmull-Rom splines along with OpenGL texture mapping to create a roller coaster simulation.

Core Credit Features: (Answer these Questions with Y/N; you can also insert comments as appropriate)
======================

1. Uses OpenGL core profile, version 3.2 or higher - Y

2. Completed all Levels:
  Level 1 : - Y
  Level 2 : - Y
  Level 3 : - Y
  Level 4 : - Y
  Level 5 : - Y

3. Used Catmull-Rom Splines to render the Track - Y

4. Rendered a Rail Cross Section - Y

5. Rendered the camera at a reasonable speed in a continuous path/orientation - Y

6. Run at interactive frame rate (>15fps at 1280 x 720) - Y

7. Understandably written, well commented code - Y
Files are fairly well documented.  
Added `constants.h` and `hw2_config.h` to make `hw2.cpp` less clustered.

8. Attached an Animation folder containing not more than 1000 screenshots - Y
Saved to `hw2/screenshots`

9. Attached this ReadMe File - Y

Extra Credit Features: (Answer these Questions with Y/N; you can also insert comments as appropriate)
======================

1. Render a T-shaped rail cross section - N

2. Render a Double Rail - Y

3. Made the track circular and closed it with C1 continuity - N

4. Added OpenGl lighting - N

5. Any Additional Scene Elements? (list them here)

6. Generate track from several sequences of splines - N

7. Draw splines using recursive subdivision - N

8. Modify velocity with which the camera moves - N

9. Create tracks that mimic a real world coaster - N

10. Render environment in a better manner - N

Additional Features: (Please document any additional features you may have implemented other than the ones described above)
1. Rollercoaster ride can be paused and unpaused with `p` key
2. While paused, toggle manual drag controls with `e` key

Open-Ended Problems: (Please document approaches to any open-ended problems that you have tackled)
1.
2.

Keyboard/Mouse controls: (Please document Keyboard/Mouse controls if any)
1. Same drag controls from `hw1` can be activated using `e` whiled paused:
	- <b>left:</b> look around, `yz` rotate
	- <b>right:</b> tilt world, `x` rotate
	- <b>ctrl left:</b> move around, `xy` translate
	- <b>ctrl right:</b> move updown, `z` translate
	- <b>scaling not supported</b>

Names of the .cpp files you made changes to:
1. `hw2.cpp`
2. `basicPipelineProgram.cpp`

Comments : (If any)


