/**
 * CSCI 420 Computer Graphics, USC 
 * Assignment 1: Height Fields
 *
 * Student username: bohuimoo
 */

/* ----- INCLUDES ----- */
#include <iostream>
#include <sstream>
#include <cstring>
#include "openGLHeader.h"
#include "glutHeader.h"

#include "imageIO.h"
#include "openGLMatrix.h"
#include "basicPipelineProgram.h"

using namespace std;


/* ----- PLATFORM ----- */
#ifdef WIN32
    #ifdef _DEBUG
        #pragma comment(lib, "glew32d.lib")
    #else
        #pragma comment(lib, "glew32.lib")
    #endif
#endif

#ifdef WIN32
    char shaderBasePath[1024] = SHADER_BASE_PATH;
#else
    char shaderBasePath[1024] = "../openGLHelper";
#endif



/* ----- DEFS ----- */
typedef struct Vec3
{
    float x;
    float y;
    float z;

    Vec3()
    {
        x = y = z = 0;
    }

    Vec3(float a, float b, float c)
    {
        x = a;
        y = b;
        z = c;
    }

    void assign(float a, float b, float c)
    {
        x = a;
        y = b;
        z = c;
    }
} Vec3;

typedef enum {
    ROTATE, TRANSLATE, SCALE
} CONTROL_STATE;

GLenum mode[3] = {
    GL_TRIANGLES, 
    GL_LINES, 
    GL_POINTS
};


/* ----- CONST ----- */
char windowTitle[512] = "CSCI 420 homework I";
int windowWidth = 1280;
int windowHeight = 720;

size_t vec3Size = sizeof(Vec3);

float ORANGE[] = {255/255.f, 185/255.f, 115/255.f};
float YELLOW[] = {255/255.f, 246/255.f, 100/255.f};
float BROWN[]  = {180/255.f, 125/255.f, 75/255.f};


/* ----- EVENT STATES ----- */
int mousePos[2]; // x,y coordinate of the mouse position

int leftMouseButton = 0; // 1 if pressed, 0 if not 
int middleMouseButton = 0; // 1 if pressed, 0 if not
int rightMouseButton = 0; // 1 if pressed, 0 if not

CONTROL_STATE controlState = ROTATE;


/* ----- VARS ----- */
ImageIO *image;
unsigned int pixelWidth = 0;
unsigned int pixelHeight = 0;
unsigned int squareWidth = 0;
unsigned int squareHeight = 0;

OpenGLMatrix *matrix;
BasicPipelineProgram *pipeline;
GLuint program;

Vec3 *vertices;
Vec3 *colors;

bool  paused = false;
float theta[3] = {0.0f, 0.0f, 0.0f};
float landScale[3] = {1.0f, 1.0f, 1.0f};
float landRotate[3] = {0.0f, 0.0f, 0.0f};
float landTranslate[3] = {0.0f, 0.0f, 0.0f};

int type = 0;
GLuint vao[3];
int vCount[3] = {0, 0, 0};

//screenshot count
int ssCount = 0;
stringstream strStream;



/* ----- FUNCTIONS ----- */
void init(int argc, char *argv[]);
void initGLEW();
void initGLUT(int argc, char *argv[]);
void initImage(char *filename);
void initLibraries();

void initSolid();
void initLines();
void initPoint();
void setupVAO(int i, const Vec3 *vertices, const Vec3 *colors);

void displayFunc();
void idleFunc();
void reshapeFunc(int w, int h);
void mouseMotionDragFunc(int x, int y);
void mouseMotionFunc(int x, int y);
void mouseButtonFunc(int button, int state, int x, int y);
void keyboardFunc(unsigned char key, int x, int y);

void saveScreenshot(const char* filename);



/* ----- INIT & SETUP ----- */
/**
 * Program must take 
 */
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "The arguments are incorrect." << endl;
        cout << "usage: ./hw1 <heightmap file>" << endl;
        exit(EXIT_FAILURE);
    }

    init(argc, argv);
    glutMainLoop();
}

/**
 * Call all necessary init functions
 */
void init(int argc, char *argv[])
{
    //cores
    initGLEW();
    initGLUT(argc, argv);

    //helper libraries
    initImage(argv[1]);
    initLibraries();

    //models
    initSolid();
    initLines();
    initPoint();
}

/**
 * Initialize GLEW depending on OS.
 */
void initGLEW()
{
    #ifdef __APPLE__
        // nothing is needed on Apple
    #else
        // Windows, Linux
        GLint result = glewInit();
        if (result != GLEW_OK)
        {
            cout << "error: " << glewGetErrorString(result) << endl;
            exit(EXIT_FAILURE);
        }
    #endif
}

/**
 * Initialize GLUT and OpenGL.
 * Set window properties and assign the main loop function selectors.
 */
void initGLUT(int argc, char *argv[])
{
    //cout << "Initializing GLUT..." << endl;
    glutInit(&argc,argv);

    //cout << "Initializing OpenGL..." << endl;
    #ifdef __APPLE__
        glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    #else
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
    #endif

    glutInitWindowSize(windowWidth, windowHeight);
    glutInitWindowPosition(0, 0);  
    glutCreateWindow(windowTitle);

    //main loop selectors
    glutDisplayFunc(displayFunc); 
    glutIdleFunc(idleFunc);
    glutMotionFunc(mouseMotionDragFunc);
    glutPassiveMotionFunc(mouseMotionFunc);
    glutMouseFunc(mouseButtonFunc);
    glutReshapeFunc(reshapeFunc);
    glutKeyboardFunc(keyboardFunc);

    cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
    cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << endl;
    cout << "Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

    //extra
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
}

/**
 * Create a new ImageIO object and load the passed in JPEG filename.
 * Pixel width/height are the resolution of the image.
 * Square width/height are dimensions for squares defined by 4 pixels.
 *
 * @param filename - path/name to JPEG file to load
 */
void initImage(char *filename)
{
    // load the image from a jpeg disk file to main memory
    image = new ImageIO();
    if (image->loadJPEG(filename) != ImageIO::OK)
    {
        cout << "Error reading image " << filename << "." << endl;
        exit(EXIT_FAILURE);
    }

    //
    pixelWidth = image->getWidth();
    pixelHeight = image->getHeight();

    squareWidth = pixelWidth - 1;
    squareHeight = pixelHeight - 1;
}

/**
 * Initialize Matrix and Shader pipeline helper libraries.
 * Direct pipeline to folder with GLSL shader files.
 * Bind to directory and get program
 */
void initLibraries()
{
    matrix = new OpenGLMatrix();

    pipeline = new BasicPipelineProgram();
    pipeline->Init("../openGLHelper-starterCode");
    pipeline->Bind();
    program = pipeline->GetProgramHandle();
}

/**
 * Initialize/setup VAO of solid surface mode (0).
 * Surface mode will be rendered with GL_TRIANGLES. 
 * Vertices: 1 square per 4 pixels, 2 triangles per square, 3 points per triangle.
 * Color will be a traditional gray scale gradient.
 */
void initSolid()
{
    int vIndex = 0;
    vCount[vIndex] = squareWidth * squareHeight * 2 * 3; //store # of vertices

    //dynamically create arrays for VBO data
    Vec3 *vertices = new Vec3[ vCount[vIndex] ];
    Vec3 *colors = new Vec3[ vCount[vIndex] ];

    float val;
    int i, j, index = 0;
    for (int x = 0; x < squareWidth; ++x)
    {
        for (int y = 0; y < squareHeight; ++y)
        {
            //save 4 corners of square
            Vec3 v[4]; //vertex values of 4 corners
            Vec3 c[4]; //color values of 4 corners
            for (int z = 0; z < 4; ++z)
            {
                switch(z)
                {
                    case 0: i = x;   j = y;   break; //bot left
                    case 1: i = x;   j = y+1; break; //top left
                    case 2: i = x+1; j = y+1; break; //top right
                    case 3: i = x+1; j = y;   break; //bot right
                    default: break;
                }

                val = image->getPixel(i,j,0) / 255.f;
                v[z].assign(2 * i/(float)pixelHeight - 1, 2 * j/(float)pixelWidth - 1, val/4);
                c[z].assign(val, val, val);
            }

            //upper triangle
            vertices[index] = v[0]; colors[index++] = c[0];
            vertices[index] = v[1]; colors[index++] = c[1];
            vertices[index] = v[2]; colors[index++] = c[2];

            //lower triangle
            vertices[index] = v[0]; colors[index++] = c[0];
            vertices[index] = v[3]; colors[index++] = c[3];
            vertices[index] = v[2]; colors[index++] = c[2];
        }
    }

    setupVAO(vIndex, vertices, colors);
}

/**
 * Initialize/setup VAO of solid surface mode (1).
 * Surface mode will be rendered with GL_LINES. 
 * Vertices: 1 square per 4 pixels, 3 lines per square (left, bot, diag), 2 points per line.
 * Color will be a black->brown gradient.
 */
void initLines()
{
    int vIndex = 1;
    vCount[vIndex] = squareWidth * squareHeight * 2 * 3; //store # of vertices

    //dynamically create arrays for VBO data
    Vec3 *vertices = new Vec3[ vCount[vIndex] ];
    Vec3 *colors = new Vec3[ vCount[vIndex] ];

    float val;
    int i, j, index = 0;
    for (int x = 0; x < squareWidth; ++x)
    {
        for (int y = 0; y < squareHeight; ++y)
        {
            //save 4 corners of square
            Vec3 v[4]; //vertex values of 4 corners
            Vec3 c[4]; //color values of 4 corners
            for (int z = 0; z < 4; ++z)
            {
                switch(z)
                {
                    case 0: i = x;   j = y;   break; //bot left
                    case 1: i = x;   j = y+1; break; //top left
                    case 2: i = x+1; j = y+1; break; //top right
                    case 3: i = x+1; j = y;   break; //bot right
                    default: break;
                }

                val = image->getPixel(i,j,0) / 255.f;
                v[z].assign(2 * i/(float)pixelHeight - 1, 2 * j/(float)pixelWidth - 1, val/4);
                c[z].assign(val * BROWN[0], val * BROWN[1], val * BROWN[2]);
            }

            //left side
            vertices[index] = v[0]; colors[index++] = c[0];
            vertices[index] = v[1]; colors[index++] = c[1];

            //diagonal
            vertices[index] = v[0]; colors[index++] = c[0];
            vertices[index] = v[2]; colors[index++] = c[2];

            //right side
            vertices[index] = v[0]; colors[index++] = c[0];
            vertices[index] = v[3]; colors[index++] = c[3];
        }
    }

    setupVAO(vIndex, vertices, colors);
}

/**
 * Initialize/setup VAO of solid surface mode (0).
 * Surface mode will be rendered with GL_POINTS. 
 * Vertices: 1 per pixel.
 * Color will be a black->yellow.
 */
void initPoint()
{
    int vIndex = 2;
    vCount[vIndex] = pixelHeight * pixelWidth; //store # of vertices

    //dynamically create arrays for VBO data
    Vec3 *vertices = new Vec3[vCount[2]];
    Vec3 *colors = new Vec3[vCount[2]];

    float val;
    int index = 0;
    for (int x = 0; x < pixelWidth; ++x)
    {
        for (int y = 0; y < pixelHeight; ++y)
        {
            val = image->getPixel(x,y,0)/255.f;
            vertices[index] = Vec3(2 * x/(float)pixelHeight - 1, 2 * y/(float)pixelWidth - 1, val/4);
            colors[index++] = Vec3(val * YELLOW[0], val * YELLOW[1], val * YELLOW[2]);
        }
    }

    setupVAO(vIndex, vertices, colors);
}

/**
 * Setup the VAO at the specified mode index, using VBO data given.
 * Generate the VAO and bind it to write data to it.
 * Generate, bind, and buffer each Vertex/Color VBOs, and link to shaders via VAO.
 * Unbind VBO, VAO, and delete arrays.
 *
 * @param i - mode index referring to render mode
 * @param vertices - dynamic array containing vertex data as Vec3 (xyz)
 * @param colors   - dynamic array containing color data as Vec3 (RBG)
 */
void setupVAO(int i, const Vec3 *vertices, const Vec3 *colors)
{
    if (i < 0 || 2 < i)
        return;

    glGenVertexArrays(1, &vao[i]); 
    glBindVertexArray(vao[i]);

    //vertices
    GLuint v_vbo;
    glGenBuffers(1, &v_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, v_vbo);
    glBufferData(GL_ARRAY_BUFFER, vCount[i] * vec3Size, vertices, GL_STATIC_DRAW);

    GLuint loc = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(loc);
    glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);


    //colors
    GLuint c_vbo;
    glGenBuffers(1, &c_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, c_vbo);
    glBufferData(GL_ARRAY_BUFFER, vCount[i] * vec3Size, colors, GL_STATIC_DRAW);

    GLuint loc2 = glGetAttribLocation(program, "color");
    glEnableVertexAttribArray(loc2);
    glVertexAttribPointer(loc2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    //clean
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    delete[] vertices;
    delete[] colors;
}



/* ----- GLUT EVENT LOOP ----- */
/**
 * Called when window is resized, which pauses display updates.
 * Based on the new window aspect, change camera perspective.
 */
void reshapeFunc(int w, int h)
{
    glViewport(0, 0, w, h);
    
    windowWidth = w;
    windowHeight = h;

    GLfloat aspect = (GLfloat) w / (GLfloat) h; 
    matrix->SetMatrixMode(OpenGLMatrix::Projection); 
    matrix->LoadIdentity(); 
    //matrix->Ortho(-2.0, 2.0, -2.0/aspect, 2.0/aspect, 0.0, 10.0); 
    matrix->Perspective(60.0, aspect, 0.01, 1000.0);
    matrix->SetMatrixMode(OpenGLMatrix::ModelView);
}

/**
 * Idle phase before displaying.
 * Rotates the landscape if animation is not paused.
 */
void idleFunc()
{
    //screenshots for submission
    // strStream.str("");
    // strStream.clear();
    // strStream << "screenshots/" << (ssCount < 10 ? "00" : (ssCount < 100 ? "0" : "")) << ssCount << ".jpg";
    // saveScreenshot(strStream.str().c_str());
    // ++ssCount;

    //rotate landscape
    if (!paused)
    {
        landRotate[2] += 1.0f;
        if (landRotate[2] >= 360.f)
            landRotate[2] = -360.f;
    }

    glutPostRedisplay();
}

/**
 * Display the generated VAOs according to user interaction & landscape state.
 * Set the camera, apply transformations, and load the matrices to GPU.
 * Bind VAO according to selected render mode, call glDrawArrays(), and swap buffer.
 */
void displayFunc()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

    matrix->SetMatrixMode(OpenGLMatrix::ModelView);
    matrix->LoadIdentity();

    //camera
    matrix->LookAt(0, -1.5, 1,  //position
                   0, 1.5, -1, //line
                   0, 1, 0); //up

    //transformations
    matrix->Translate(landTranslate[0], landTranslate[1], landTranslate[2]);
    matrix->Scale(landScale[0], landScale[1], landScale[2]);
    if (landRotate[0] != 0) matrix->Rotate(landRotate[0]/2, 1.0, 0.0, 0.0);
    if (landRotate[1] != 0) matrix->Rotate(landRotate[1]/2, 0.0, 1.0, 0.0);
    if (landRotate[2] != 0) matrix->Rotate(landRotate[2]/2, 0.0, 0.0, 1.0);

    //model
    GLint h_modelViewMatrix = glGetUniformLocation(program, "modelViewMatrix");
    float m[4 * 4];
    matrix->GetMatrix(m);
    glUniformMatrix4fv(h_modelViewMatrix, 1, GL_FALSE, m);

    //projection
    matrix->SetMatrixMode(OpenGLMatrix::Projection);
    GLint h_projectionMatrix = glGetUniformLocation(program, "projectionMatrix");
    float p[4 * 4];
    matrix->GetMatrix(p);
    glUniformMatrix4fv(h_projectionMatrix, 1, GL_FALSE, p);

    //bind vao & render
    glBindVertexArray(vao[type]);
    glDrawArrays(mode[type], 0, vCount[type]);
    glBindVertexArray(0);

    glutSwapBuffers();
}

/**
 * Called when user presses a key.
 * Actions based on keys:
 *  1       change to Solid mode
 *  2       change to Wireframe mode
 *  3       change to Points mode
 *  x       save screenshot
 *  p       toggle rotation
 *  ESC     exit program
 *
 * @param key - pressed key
 */
void keyboardFunc(unsigned char key, int x, int y)
{
    switch (key)
    {  
        //change render mode
        case '1': type = 0; break;
        case '2': type = 1; break;
        case '3': type = 2; break;

        //pause rotation
        case 'p':
        case 'P': paused = !paused; break;

        //screenshot
        case 'x':
        case 'X': saveScreenshot("screenshot.jpg"); break;

        //ESC: quit program
        case 27: exit(0);
    }
}

/**
 * Store new position of mouse, for calculating delta mouse move
 */
void mouseMotionFunc(int x, int y)
{
    mousePos[0] = x;
    mousePos[1] = y;
}

/**
 * When mouse button has been pressed/released.
 * Store mouse button & modifier keys (CTRL/SHIFT) states
 */
void mouseButtonFunc(int button, int state, int x, int y)
{   
    //track mouse button state: leftMouseButton, middleMouseButton, rightMouseButton
    switch (button)
    {
        case GLUT_LEFT_BUTTON:
            leftMouseButton = (state == GLUT_DOWN);
            break;
        case GLUT_MIDDLE_BUTTON:
            middleMouseButton = (state == GLUT_DOWN);
            break;
        case GLUT_RIGHT_BUTTON:
            rightMouseButton = (state == GLUT_DOWN);
        break;
    }

    // keep track of whether CTRL and SHIFT keys are pressed
    switch (glutGetModifiers())
    {
        case GLUT_ACTIVE_CTRL:
            controlState = TRANSLATE;
            break;
        case GLUT_ACTIVE_SHIFT:
            controlState = SCALE;
            break;
        default: //if CTRL and SHIFT are not pressed, we are in rotate mode
            controlState = ROTATE;
            break;
    }

    // store the new mouse position
    mousePos[0] = x;
    mousePos[1] = y;
}

/**
 * Called when mouse is moved while pressing a button.
 * Depending on both currently pressed key modifier & mouse movement, modify transformation matrix.
 */
void mouseMotionDragFunc(int x, int y)
{
    //get position delta
    int mousePosDelta[2] = { x - mousePos[0], y - mousePos[1] };

    switch (controlState)
    {
        // translate the landscape
        case TRANSLATE:
            if (leftMouseButton)
            {
                // control x,y translation via the left mouse button
                landTranslate[0] += mousePosDelta[0] * 0.01f;
                landTranslate[1] -= mousePosDelta[1] * 0.01f;
            }
            if (rightMouseButton) {
                landTranslate[2] -= mousePosDelta[1] * 0.01f;
            }
            break;
        case ROTATE:
            if (leftMouseButton)
            {
                // control x,y rotation via the left mouse button
                landRotate[0] += mousePosDelta[1];
                landRotate[1] += mousePosDelta[0];
            }
            if (rightMouseButton) {
                landRotate[2] += mousePosDelta[0];
            }
            break;
        case SCALE:
            if (leftMouseButton)
            {
                // control x,y scaling via the left mouse button
                landScale[0] *= 1.0f + mousePosDelta[0] * 0.01f;
                landScale[1] *= 1.0f - mousePosDelta[1] * 0.01f;
            }
            if (rightMouseButton) {
                landScale[2] *= 1.0f - mousePosDelta[1] * 0.01f;
            }
            break;
    }

    // store the new mouse position
    mousePos[0] = x;
    mousePos[1] = y;
}


/* ----- HELPERS ----- */
/**
 * write a screenshot to the specified filename
 */
void saveScreenshot(const char *filename)
{
    unsigned char * screenshotData = new unsigned char[windowWidth * windowHeight * 3];
    glReadPixels(0, 0, windowWidth, windowHeight, GL_RGB, GL_UNSIGNED_BYTE, screenshotData);

    ImageIO screenshotImg(windowWidth, windowHeight, 3, screenshotData);

    if (screenshotImg.save(filename, ImageIO::FORMAT_JPEG) == ImageIO::OK)
        cout << "File " << filename << " saved successfully." << endl;
    else 
        cout << "Failed to save file " << filename << '.' << endl;

    delete[] screenshotData;
}

