CSCI 420 HW 1
=============

Bohui Moon, bohuimoo  


How to run:
```bash
$ cd Assignment1/hw1
$ make
$ ./hw1 <path to image>.jpg
```


Notes:
- Program requires as second argument to run
- Image must be grayscale
- 300 screenshots of working program is provided under `/hw1/screenshots`


Features:
- Provided image is rendered as a landscape
- Rendered in three modes: solid surface, wireframe, points
- Use number keys `1, 2, 3` to switch render modes
- Image rotates by default, use `p` key to toggle
- Landscape can be transformed
	- `SHIFT + Left Mouse`: xy scaling
	- `SHIFT + Right Mouse`: z scaling
	- `CTRL  + Left Mouse`: xy translation
	- `CTRL  + Right Mouse`: z translation
	- `Left Mouse`: xy rotation
	- `Right Mouse`: z rotation