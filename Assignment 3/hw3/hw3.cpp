/* **************************
 * CSCI 420
 * Assignment 3 Raytracer
 * Name: Bohui Moon, bohuimoo 9937533702
 * *************************
*/

/* --- HEADER --- */
#ifdef WIN32
    #include <windows.h>
#endif

#if defined(WIN32) || defined(linux)
    #include <GL/gl.h>
    #include <GL/glut.h>
#elif defined(__APPLE__)
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef WIN32
    #define strcasecmp _stricmp
#endif

#include <iostream>
#include <cmath>
#include <cstdint>
#include <imageIO.h>

#include <glm/glm.hpp>

using namespace std;
using namespace glm;

typedef unsigned int uint;


/* --- CONST --- */
#define MAX_TRIANGLES 20000
#define MAX_SPHERES 100
#define MAX_LIGHTS 100

char * filename = NULL;

//different display modes
#define MODE_DISPLAY 1
#define MODE_JPEG 2

int mode = MODE_DISPLAY;

//you may want to make these smaller for debugging purposes
#define WIDTH 640
#define HEIGHT 480
double ASPECT = WIDTH / (double) HEIGHT;

//the field of view of the camera
#define FOV_DEG 60.0
double FOV = M_PI * (FOV_DEG / 180.0);


/* --- DEFS --- */
enum ShapeType { NONE, SPHERE, TRIANGLE };

struct Ray
{
    //src & dir
    dvec3 p;
    dvec3 v;

    //intersection
    dvec3 point;
    double t;

    ShapeType type;
    uint index;

    Ray(dvec3 src, dvec3 dir)
    {
        p = src;
        v = dir;

        type = NONE;
        index = 0;
        t = 0;
    }

    void intersect(double tval, ShapeType st, uint i)
    {
        type = st;
        index = i;

        t = tval;
        point = p + (t * v);
    }
};

struct Vertex
{
    dvec3 p;  //position
    dvec3 n;  //normal
    dvec3 kd; //color diffuse
    dvec3 ks; //color specular
    double shininess;
};

struct Triangle
{
    Vertex v[3];

    dvec3 normal()
    {
        return normalize( cross(v[2].p - v[0].p, v[1].p - v[0].p) );
    }

    dvec3 barycentric(dvec3 &p)
    {
        dvec3 v0 = v[2].p - v[0].p;
        dvec3 v1 = v[1].p - v[0].p;
        dvec3 v2 = p - v[0].p;

        double dot00 = dot(v0, v0);
        double dot01 = dot(v0, v1);
        double dot02 = dot(v0, v2);
        double dot11 = dot(v1, v1);
        double dot12 = dot(v1, v2);

        double inv = (dot00 * dot11) - (dot01 * dot01);
        double z = (dot11 * dot02 - dot01 * dot12) / inv;
        double y = (dot00 * dot12 - dot01 * dot02) / inv;
        double x = 1.0 - z - y;
        
        return dvec3(x, y, z);
    }
};

struct Sphere
{
    double r;      //radius
    dvec3 p;  //position
    dvec3 kd; //color diffuse
    dvec3 ks; //color specular
    double shininess;

    dvec3 normalAt(dvec3 point)
    {
        return glm::normalize(point - p);
    }
};

struct Light
{
    dvec3 p; //position
    dvec3 c; //color
};


/* --- SCENE --- */
unsigned char buffer[HEIGHT][WIDTH][3];

uint num_triangles = 0;
Triangle triangles[MAX_TRIANGLES];

uint num_spheres = 0;
Sphere spheres[MAX_SPHERES];

uint num_lights = 0;
Light lights[MAX_LIGHTS];

dvec3 ambient;



/* FUNCTIONS */
void init();
int loadScene(char *argv);
void parse_check(const char *expected, char *found);
void parse_doubles(FILE* file, const char *check, dvec3 &p);
void parse_rad(FILE *file, double *r);
void parse_shi(FILE *file, double *shi);

void idle();
void display();

void draw_scene();

double clamp(double val, double min, double max);
double raySphereIntersect(Ray &ray, Sphere &s);
void   calculateIntersect(Ray &ray, ShapeType exType = NONE, uint exIndex = 0);
dvec3  calculateLightings(Ray &ray);


void plot_pixel(int x,int y,unsigned char r,unsigned char g,unsigned char b);
void plot_pixel_display(int x,int y,unsigned char r,unsigned char g,unsigned char b);
void plot_pixel_jpeg(int x,int y,unsigned char r,unsigned char g,unsigned char b);

void save_jpg();
void printVec(const char *prepend, dvec3 &v);



/* ----- SETUP ----- */
/**
 * Main
 */
int main(int argc, char* argv[])
{
    if ((argc < 2) || (argc > 3))
    {  
        printf ("Usage: %s <input scenefile> [output jpegname]\n", argv[0]);
        exit(0);
    }
  
    if(argc == 3)
    {
        mode = MODE_JPEG;
        filename = argv[2];
    }
    else if(argc == 2)
        mode = MODE_DISPLAY;

    glutInit(&argc,argv);
    loadScene(argv[1]);

    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("Ray Tracer");

    glutDisplayFunc(display);
    glutIdleFunc(idle);
    
    init();
    glutMainLoop();
}

/** 
 * Init camera & gl settings
 */
void init()
{
    glMatrixMode(GL_PROJECTION);
    glOrtho(0, WIDTH, 0, HEIGHT, 1, -1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);
}

/**
 * Load scene described by given filename.
 * Parses triangles, spheres, and light sources.
 */
int loadScene(char *argv)
{
    FILE * file = fopen(argv, "r");
    
    char type[50];
    Triangle t;
    Sphere s;
    Light l;

    int number_of_objects;
    fscanf(file, "%i", &number_of_objects);
    printf("number of objects: %i\n", number_of_objects);

    parse_doubles(file, "amb:", ambient);

    for(int i=0; i < number_of_objects; ++i)
    {
        fscanf(file, "%s\n", type);
        printf("%s\n", type);
    
        if(strcasecmp(type, "triangle") == 0)
        {
            printf("found triangle\n");
      
            for(int j=0;j < 3;j++)
            {
                parse_doubles(file, "pos:", t.v[j].p);
                parse_doubles(file, "nor:", t.v[j].n);
                parse_doubles(file, "dif:", t.v[j].kd);
                parse_doubles(file, "spe:", t.v[j].ks);
                parse_shi(file, &t.v[j].shininess);
            }

            if(num_triangles == MAX_TRIANGLES)
            {
                printf("too many triangles, you should increase MAX_TRIANGLES!\n");
                exit(0);
            }
      
            triangles[num_triangles++] = t;
        }
        else if(strcasecmp(type, "sphere") == 0)
        {
            printf("found sphere\n");

            parse_doubles(file, "pos:", s.p);
            parse_rad(file, &s.r);
            parse_doubles(file, "dif:", s.kd);
            parse_doubles(file, "spe:", s.ks);
            parse_shi(file, &s.shininess);

            if(num_spheres == MAX_SPHERES)
            {
                printf("too many spheres, you should increase MAX_SPHERES!\n");
                exit(0);
            }
      
            spheres[num_spheres++] = s;
        }
        else if(strcasecmp(type, "light") == 0)
        {
            printf("found light\n");
            parse_doubles(file, "pos:", l.p);
            parse_doubles(file, "col:", l.c);

            if(num_lights == MAX_LIGHTS)
            {
                printf("too many lights, you should increase MAX_LIGHTS!\n");
                exit(0);
            }
            
            lights[num_lights++] = l;
        }
        else
        {
            printf("unknown type in scene description:\n%s\n",type);
            exit(0);
        }
    }

    return 0;
}

/**
 * Ensure valid foramt when parsing.
 */
void parse_check(const char *expected, char *found)
{
    if(strcasecmp(expected, found))
    {
        printf("Expected '%s ' found '%s '\n", expected, found);
        printf("Parse error, abnormal abortion\n");
        exit(0);
    }
}

/**
 * Parse dobules
 */
void parse_doubles(FILE* file, const char *check, dvec3 &p)
{
    char str[100];
    fscanf(file, "%s", str);
    parse_check(check, str);
    fscanf(file, "%lf %lf %lf", &p.x, &p.y, &p.z);// &p[0], &p[1], &p[2]);
    printf("%s %lf %lf %lf\n", check, p.x, p.y, p.z);
}

/**
 * Parse radians
 */
void parse_rad(FILE *file, double *r)
{
    char str[100];
    fscanf(file, "%s", str);
    parse_check("rad:", str);
    fscanf(file, "%lf", r);
    printf("rad: %f\n", *r);
}

/**
 * Parse shininess
 */
void parse_shi(FILE *file, double *shi)
{
    char s[100];
    fscanf(file, "%s", s);
    parse_check("shi:", s);
    fscanf(file,"%lf", shi);
    printf("shi: %f\n", *shi);
}



/* ----- EVENT LOOP -----*/
/**
 * Idle
 */
void idle()
{
    //hack to make it only draw once
    static int once = 0;
    
    if(!once)
    {
        draw_scene();

        if(mode == MODE_JPEG)
            save_jpg();
    }
    once = 1;
}

/**
 * Display
 */
void display()
{
    //TODO
}



/* ----- RENDER ----- */
/**
 *
 */
void draw_scene()
{
    //TODO: MODIFY THIS FUNCTION
    double tl[] = {-ASPECT * tan(FOV/2),  tan(FOV/2), -1};
    double tr[] = { ASPECT * tan(FOV/2),  tan(FOV/2), -1};
    double bl[] = {-ASPECT * tan(FOV/2), -tan(FOV/2), -1};
    double br[] = { ASPECT * tan(FOV/2), -tan(FOV/2), -1};

    double delx = (tr[0] - tl[0])/WIDTH;
    double dely = (tl[1] - bl[1])/HEIGHT;


    //start rendering mode
    glPointSize(2.0);  
    glBegin(GL_POINTS);

    for(uint y = 0; y < HEIGHT; ++y)
    {
        for(uint x = 0; x < WIDTH; ++x)
        {
            //get ray from camera
            Ray ray(dvec3(0), normalize( dvec3(bl[0] + (x * delx), bl[1] + (y * dely), -1.0) ));

            calculateIntersect(ray);
            dvec3 color = calculateLightings(ray);
            plot_pixel(x, y, color[0], color[1], color[2]);
        }
    }
    glEnd();
    glFlush();
    printf("Done!\n"); fflush(stdout);
}

/**
 * Clamp val to [min, max]
 */
double clamp(double val, double min, double max)
{
    return fmin(fmax(val, min), max);
}

/**
 * Returns the closer intersection t value between given ray and sphere.
 */
double raySphereIntersect(Ray &ray, Sphere &sphere)
{
    double diff_x = ray.p.x - sphere.p.x;
    double diff_y = ray.p.y - sphere.p.y;
    double diff_z = ray.p.z - sphere.p.z;

    double b = 2 * ( (ray.v.x * diff_x) + (ray.v.y * diff_y) + (ray.v.z * diff_z) );
    double c = pow(diff_x, 2) + pow(diff_y, 2) + pow(diff_z, 2) - pow(sphere.r, 2);

    double b24c = (b * b) - (4 * c);
    if (b24c < 0)
        return -1;

    double t0 = ( -b + sqrt(b24c) ) / 2.0;
    double t1 = ( -b - sqrt(b24c) ) / 2.0;

    if (t0 <= 0 && t1 <= 0)
        return -1;

    if (t0 <= 0 && t1 > 0)
        return t1;

    if (t0 > 0 && t1 <= 0)
        return t0;

    return fmin(t0, t1);
}

/**
 * Returns intersection t value between ray and triangle.
 * Checks for not inside:
 * - Ray direction is parallel to plane normal
 * - Intersection of ray & plane is behind/invalid 
 * - Use baycentric method for final check within triangle
 */
double rayTriangleIntersect(Ray &ray, Triangle &triangle)
{
    Vertex *vertices = triangle.v;

    dvec3 planeNormal = triangle.normal();
    double nd = dot(planeNormal, ray.v);
    if (nd == 0)
        return -1;

    //get intersection
    double t = dot(planeNormal, vertices[0].p - ray.p) / nd;
    dvec3 p = ray.p + (ray.v * t);
    if (t < 0 || (p != p))
        return -1;

    //baycentric point in triangle check [http://www.blackpawn.com/texts/pointinpoly/]
    dvec3 bc = triangle.barycentric(p);

    bool inside = (bc[2] >= 0) && (bc[1] >= 0) && (bc[2] + bc[1] < 1);
    return inside ? t : -1;
}

/**
 * Calculate and modify ray's intersection point data.
 * Exclude given surface (specified by type and index), from intersection calculations.
 */
void calculateIntersect(Ray &ray, ShapeType exType, uint exIndex)
{
    double t;

    //spheres
    for (uint i = 0; i < num_spheres; ++i)
    {
        if (! (exType == SPHERE && exIndex == i))
        {
            t = raySphereIntersect(ray, spheres[i]);

            if (t >= 0 && (t < ray.t || ray.type == NONE))
                ray.intersect(t, SPHERE, i);
        }
    }

    //triangles
    for (uint i = 0; i < num_triangles; ++i)
    {
        if (! (exType == TRIANGLE && exIndex == i))
        {
            t = rayTriangleIntersect(ray, triangles[i]);

            if (t >= 0 && (t < ray.t || ray.type == NONE))
                ray.intersect(t, TRIANGLE, i);
        }
    }
}

/**
 * Calculate the lighting for the intersection point of given ray.
 * For each light source, shoot a shadow ray.
 * If blocking objects, add lighting from source to final color.
 * Return final color for intersection point in current ray.
 */
dvec3 calculateLightings(Ray &ray)
{
    //no intersection, paint white
    if (ray.type == NONE)
        return dvec3(255);

    //lights
    dvec3 color;

    for (uint i = 0; i < num_lights; ++i)
    {
        Light light = lights[i];

        //independent of shape
        dvec3 v = -ray.v;
        dvec3 l = normalize(light.p - ray.point);

        Ray shadowRay(ray.point, l);
        calculateIntersect(shadowRay, ray.type, ray.index);

        if ( shadowRay.type == NONE || distance(ray.point, shadowRay.point) >= distance(ray.point, light.p) ) 
        {
            //dependent of shape
            dvec3 n, kd, ks;
            double a = 0;;
            if (ray.type == SPHERE)
            {
                Sphere sp = spheres[ray.index];
                a = sp.shininess;
                n = sp.normalAt(ray.point);
                kd = sp.kd;
                ks = sp.ks;
            }
            else if (ray.type == TRIANGLE)
            {
                Triangle tr = triangles[ray.index];
                dvec3 bc = tr.barycentric(ray.point);

                for (uint v = 0; v < 3; ++v)
                {
                    a += bc[v] * tr.v[v].shininess;
                    n += bc[v] * tr.v[v].n;
                    kd += bc[v] * tr.v[v].kd;
                    ks += bc[v] * tr.v[v].ks;
                }
            }

            n = normalize(n);

            //ln, r, rv, rva
            double ln = dot(l, n); 
            if (ln < 0) ln = 0;

            dvec3 r = (2 * ln * n) - l;

            double rv = dot(r, v); 
            if (rv < 0) rv = 0;

            double rva = pow(rv, a);

            //calc & add color
            for (uint c = 0; c < 3; c++)
                color[c] += light.c[c] * ((kd[c] * ln) + (ks[c] * rva));
        }
    }

    //ambient, clamp, scale
    for (uint c = 0; c < 3; c++)
        color[c] = clamp(color[c] + ambient[c], 0.0, 1.0) * 255.0;

    return color;
}


/**
 * Plot pixel display, then jpeg if on mode
 */
void plot_pixel(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
    plot_pixel_display(x,y,r,g,b);
  
    if(mode == MODE_JPEG)
        plot_pixel_jpeg(x,y,r,g,b);
}

/**
 * 
 */
void plot_pixel_display(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
    glColor3f(((float)r) / 255.0f, ((float)g) / 255.0f, ((float)b) / 255.0f);
    glVertex2i(x,y);
}

/**
 * 
 */
void plot_pixel_jpeg(int x, int y, unsigned char r, unsigned char g, unsigned char b)
{
    buffer[y][x][0] = r;
    buffer[y][x][1] = g;
    buffer[y][x][2] = b;
}



/* ----- HELPERS ----- */
/**
 * Print the given vector in format
 */
void printVec(const char *prepend, dvec3 &v)
{
    cout << prepend << "<" << v.x << ", " << v.y << ", " << v.z << ">" << endl;
}

/**
 * Save contents of the current buffer as JPEG with name specified during program start.
 */
void save_jpg()
{
    printf("Saving JPEG file: %s\n", filename);

    ImageIO img(WIDTH, HEIGHT, 3, &buffer[0][0][0]);
  
    if (img.save(filename, ImageIO::FORMAT_JPEG) != ImageIO::OK)
        printf("Error in Saving\n");
    else 
        printf("File saved Successfully\n");
}



